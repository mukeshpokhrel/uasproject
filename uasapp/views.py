from django.shortcuts import render,redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout 
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q




class HomeView(TemplateView):
	template_name="home.html"

class RegistrationView(FormView):
	template_name="registration.html"
	form_class=RegistrationForm
	success_url="/"

	def form_valid(self,form):
		a=form.cleaned_data["username"]
		b=form.cleaned_data["email"]
		c=form.cleaned_data["password"]
		User.objects.create_user(a,b,c)


		return super().form_valid(form)

class LoginView(FormView):
	template_name="login.html"
	form_class=LoginForm
	success_url="/"

	def form_valid(self,form):
		a=form.cleaned_data["username"]
		b=form.cleaned_data["password"]
		user=authenticate(username=a,password=b)
		if user is not None:
			login(self.request,user)
		else: 
			return render(self.request,"login.html",{"error":"INVALID USERNAME OR PASSWORD","form":form})
		return super().form_valid(form)


class LogoutView(View):
	def get(self,request):
		logout(request)
		return redirect("/")

class ProtectedView(LoginRequiredMixin,TemplateView):  
	#LoginRequiredMixin:This View requires login 
	template_name="protected.html"
	login_url="/login/"

class RestrictedView(TemplateView):
	template_name="restricted.html"

	def dispatch(self,request,*args,**kwargs):
		usr=request.user
		if usr.is_authenticated and usr.username=="dipak":
			pass
		else:
			return redirect("/login/")


		return super().dispatch(request,*args,**kwargs)

class ArticleListView(ListView):
	template_name="articlelist.html"
	queryset=Article.objects.all().order_by("-id")
	context_object_name="allarticles"

class ArticleDetailView(DetailView):
	template_name="articledetail.html"
	model=Article
	context_object_name="articleobject"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		article_id=self.kwargs["pk"]
		article_object=Article.objects.get(id=article_id)
		# article_object=self.object (means line 82&83)
		article_object.view_count+=1
		article_object.save()

		return context



class ArticleCreateView(CreateView):
	template_name="addnewarticle.html"
	form_class=ArticleForm
	success_url="/article/list/"

	def dispatch(self,request,*args,**kwargs):
		usr=request.user
		if usr.is_authenticated:
			pass
		else:
			return redirect("/login/")


		return super().dispatch(request,*args,**kwargs)


	def form_valid(self,form):
		logged_in_user=self.request.user
		form.instance.author=logged_in_user
		return super().form_valid(form)

class SearchView(TemplateView):
	template_name="search.html"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		query=self.request.GET["keyword"]
		results=Article.objects.filter(Q(title__icontains=query)
			|Q(contents__icontains=query)|Q(author__username__icontains=query))
		context["search_results"]=results



		return context

class ArticleDetailView(DetailView):
	template_name="articledetail.html"
	model=Article
	context_object_name="articleobject"

	def get_context_data(self,**kwargs):
		context=super().get_context_data(**kwargs)
		article_id=self.kwargs["pk"]
		article_object=Article.objects.get(id=article_id)
		# article_object=self.object (means line 82&83)
		article_object.view_count+=1
		article_object.save()

		return context