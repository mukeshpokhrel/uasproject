from django import forms
from .models import *
from django.contrib.auth.models import User 


class RegistrationForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	email=forms.CharField(widget=forms.EmailInput())
	password=forms.CharField(widget=forms.PasswordInput())
	confirm_password=forms.CharField(widget=forms.PasswordInput())

	def clean_confirm_password(self):
		a=self.cleaned_data["password"]
		b=self.cleaned_data["confirm_password"]

		if a!=b:
			raise forms.ValidationError("password didn't match")
		return b

	def clean_username(self):
		a=self.cleaned_data["username"]
		if User.objects.filter(username=a).exists():
			raise forms.ValidationError("User with this username already exists.please choose different username.")

		return a



class LoginForm(forms.Form):
	username=forms.CharField(widget=forms.TextInput())
	password=forms.CharField(widget=forms.PasswordInput())

class ArticleForm(forms.ModelForm):
	class Meta:
		model=Article
		fields=["title","image","contents"]
		
