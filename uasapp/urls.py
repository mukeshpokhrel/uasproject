from django.urls import path
from .views import *

app_name="uasapp"
urlpatterns=[
path("",HomeView.as_view()),
path("register/",RegistrationView.as_view()),
path("login/",LoginView.as_view()),
path("logout/",LogoutView.as_view()),
path("protected/",ProtectedView.as_view()),
path("restricted/",RestrictedView.as_view()),
path("article/list/",ArticleListView.as_view()),
path("article/<int:pk>/detail/",ArticleDetailView.as_view()),
path("article/create/",ArticleCreateView.as_view()),
path("search/",SearchView.as_view(),name="search"),



]