from django.db import models
from django.contrib.auth.models import User

class Article(models.Model):
	title=models.CharField(max_length=200)
	image=models.ImageField(upload_to="article")
	contents=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	author=models.ForeignKey(User,on_delete=models.CASCADE)
	view_count=models.PositiveIntegerField(default=0)
	#foreignkey:many articles by 1 user(db_relation)

	def __str__(self):
		return self.title
	#__str__ method for constructor


class Comment(models.Model):
	commentor=models.ForeignKey(User,on_delete=models.CASCADE)
	comment=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	article=models.ForeignKey(Article,on_delete=models.CASCADE)

	def __str__(self):
		return self.commentor.username